<?php
//This is the accordion container
/*
function accordionGroup( $atts, $content=null )
{
  $atts_accordion = (shortcode_atts(array(
    'id' => '',
  ), $atts));
  return "<div class='panel-group' id=".$atts_accordion['id']." role='tablist' aria-multiselectable='true'>".do_shortcode($content)."</div>";
}
add_shortcode( 'accordion-group', 'accordionGroup');

//This is the accordion item. It's need a reference from the accordion ID
function accordionItem( $atts, $content = null)
{
  $atts_item = (shortcode_atts(array(
    'titulo' => '',
    'id' => '',
  ), $atts));

  return "
    <div class='panel panel-default'>
      <div class='panel-heading' role='tab' id=".$atts_item['id'].">
        <h4 class='panel-title'>
          <a class='collapsed' role='button' data-toggle='collapse' data-parent='#".$atts_accordion['id']."' href='#".$atts_item['id']."-collapse' aria-expanded='false' aria-controls=".$atts_item['id']."-collapse'>".$atts_item['titulo']."</a>
        </h4>
      </div>
      <div id='".$atts_item['id']."-collapse' class='panel-collapse collapse' role='tabpanel' aria-labelledby=".$atts_item['id'].">
        <div class='panel-body'>".$content."</div>
      </div>
    </div>";
}
add_shortcode( 'accordion-item', 'accordionItem');*/

class accordion
{
    public $atts, $content, $content_id;
    public function __construct()
    {
        $this->content_id = $this->randomize(3);
    }
    public function accordionGroup($atts, $content = null)
    {
        return "<div class='panel-group' id='".$this->content_id."' role='tablist' aria-multiselectable='true'>"
            .do_shortcode($content)."
        </div>";
    }
    public function accordionItem($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'titulo' => '',
            'id' => '',
        ), $atts));
        return "<div class='panel panel-default'>
            <div class='panel-heading' role='tab' id=".$id.">
                <h4 class='panel-title'>
                    <a class='collapsed' role='button' data-toggle='collapse' data-parent='#".$this->content_id."' href='#".$id."-collapse' aria-expanded='false' aria-controls=".$id."-collapse'>".$titulo."</a>
                </h4>
            </div>
            <div id='".$id."-collapse' class='panel-collapse collapse' role='tabpanel' aria-labelledby=".$id.">
                <div class='panel-body'>".$content."</div>
            </div>
        </div>";
    }
    public function randomize($rounds)
    {
        if (!isset($rand))
            $rand = '';
        for ($i = 1; $i <= $rounds; $i++) {
            $rand = md5($rand . time() . rand(0, 10000));
        }
        return $rand;
    }
}
add_shortcode('accordion-group', array('accordion', 'accordionGroup'));
add_shortcode('accordion-item', array('accordion', 'accordionItem'));
?>
