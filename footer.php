		</div>
		<footer>
			<hr>
			<img src="<?php echo IMAGES ?>/banner_pie.jpg" class="img-responsive">
		</footer>
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
			<script src="<?php echo JS ?>/bootstrap.min.js"></script>
			<script src="<?php echo JS ?>/jquery.flexslider.js"></script>
			<script type="text/javascript">
				$(window).load(function(){

					$('.slide-peliculas').flexslider({
						animation: "slide",
						animationLoop: true,
						randomize: true,
						controlNav: false,
						direction: 'horizontal',
						itemWidth: 172,
						itemMargin: 5,
					});

					$('.slide-noticias').flexslider({
						animation: "slide",
						animationLoop: true,
						controlNav: true,
						direction: 'horizontal',
					});

					$('.slide-peliculas-sidebar').flexslider({
						animation: 'slide',
						animationLoop: true,
						reverse: true,
						randomize: true,
						slideshow: true,
						direction: 'vertical',
						itemMargin: 5,
					});
				});
			</script>
		<?php wp_footer(); ?>
	</body>
</html>
