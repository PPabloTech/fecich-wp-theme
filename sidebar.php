<aside>
	<div class="col-md-3 col-lg-3 columna-publicidad">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="box">
						<h4 class="text-center"><strong>Entrada liberada</strong></h4>
						<p class="text-justify">
							Para asistir a las funciones de Quilpué, se deben retirar las entradas en el Domo FECICH (Plaza Eugenio Rengifo).
								<br>
							Para Villa Alemana, se pueden retirar en el Centro Cultural Gabriela Mistral (Santiago #674).
						</p>
			  	</div>
					<hr>
					<div class="box">
			  		<div class="fb-page" data-href="https://www.facebook.com/fecich" data-width="260" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/fecich"><a href="https://www.facebook.com/fecich">Festival de Cine Chileno (FECICH)</a></blockquote></div></div>
			  	</div>
					<hr>
					<div class="box">
						<a class="twitter-timeline" href="https://twitter.com/Fecich" data-widget-id="652870498980876288">Tweets por el @Fecich.</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</aside>
