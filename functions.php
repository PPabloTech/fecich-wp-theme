<?php
	include('shortcodes.php');
	require_once('wp_bootstrap_navwalker.php');
	require_once('wp_bootstrap_pagination.php');

	//Constantes
	define(TEMPPATH, get_template_directory_uri() );
	define(IMAGES, TEMPPATH.'/images');
	define(JS, TEMPPATH.'/js');

	add_theme_support( 'custom-header' , array(
		'default-image' => get_template_directory_uri() . '/images/header.jpg'
		));
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );

	//Menus
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'THEMENAME' ),
	) );

	//Tamaño imagenes
	add_image_size( 'cabecera-articulo', 960, 360, true );
	add_image_size( 'imagen-index', 400, 225, true );
	add_image_size( 'slide-peliculas', 172, 230, true );
	add_image_size( 'imagen-categoria', 400, 275, true );
	add_image_size( 'afiche-pelicula', 307, 410, true );

	//Limite caracteres para el Excerpt
	function custom_excerpt_length( $length )
	{
		return 25;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	//Agregar el 'Leer mas' para el Excerpt
	function new_excerpt_more( $more )
	{
		return ' ... <a class="read-more" href="' . get_permalink( get_the_ID() ) . '">' . __( 'Seguir leyendo', 'your-text-domain' ) . '</a>';
	}
	add_filter( 'excerpt_more', 'new_excerpt_more' );
?>
