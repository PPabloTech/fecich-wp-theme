<?php get_header(); ?>
<div class="col-lg-9 col-md-9 columna-contenido">
	<article>
			<section id="titulo">
				<h1><?php the_title(); ?></h1>
			</section>
				<?php
				//Empieza el loop. Es porque the_content(); no funciona fuera del loop
					while ( have_posts() ) : the_post();
					//Si hay una imagen destacada, la muestra
						if ( has_post_thumbnail() ) : the_post_thumbnail( 'cabecera-articulo', array(
								'class' => 'img-responsive img-rounded',
						));
						endif;
					?>

			<hr>
			<?php
				//Muestra el contenido
				the_content();
			endwhile;
			wp_reset_query();
		?>
	</article>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
