<?php get_header(); ?>
<div class="col-lg-9 col-md-9 columna-contenido">
		<main role="main">
			<div class="row bloque-noticias">
				<?php
					/* Queries personalizadas para cada categoria y/o seccion */

					$largometrajes = new WP_Query(array(
						'post_type'      => 'page',
						'category_name'  => 'largometrajes',
						'posts_per_page' => -1,
					));

					$cortometrajes = new WP_Query(array(
						'post_type'      => 'page',
						'category_name'  => 'cortometrajes',
						'posts_per_page' => -1,
					));

					$muestras = new WP_Query(array(
						'post_type'      => 'page',
						'category_name'  => 'muestras',
						'posts_per_page' => -1,
					));

					$noticias = new WP_Query(array(
						'post_type' 		 => 'post',
	          'showposts'      => 3,
	          'category_name'  => 'noticias',
						'posts_per_page' => -1,
					));

					$articulos = new WP_Query(array(
						'post_type'		   => 'post',
						'showposts'      => 5,
						'category_name'  =>  'noticias',
						'posts_per_page' => -1,
					));

				?>
				<div class="row flexslider slide-noticias">
					<ul class="slides">
						<?php if ( $articulos->have_posts() and is_home() ) : while ( $articulos->have_posts() ) : $articulos->the_post(); ?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( 'cabecera-articulo' ); ?>
								<h3 class="flex-caption"><?php the_title(); ?></h3>
							</a>
						</li>
					<?php
						endwhile;
						endif;
						wp_reset_query();
					?>
					</ul>
				</div>
				<hr>
				<section class="titulo">
					<a href="<?php echo get_category_link('3'); ?>">Noticias</a>
				</section>
				<div class="row articulos-noticia">
					<?php if ( $noticias->have_posts() and is_home() ) : while ( $noticias->have_posts() ) : $noticias->the_post(); ?>
						<article>
							<div class="col-md-4 col-sm-6 noticia">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('imagen-index', array(
										'class' => 'img-thumbnail img-responsive',
									)); ?>
								</a>
								<p class="fecha-publicacion"><?php the_time('j F, Y'); ?></p>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</div>
						</article>
					<?php
						endwhile;
					endif;
					wp_reset_query();
					?>
				</div>
				<hr>
				<div class="row flexslider slide-peliculas" id="slide-muestra">
					<section>
						<h3>Muestra Nacional de Largometrajes</h3>
					</section>
					<ul class="slides">
							<?php if ( $muestras->have_posts() and is_home() ) : while ( $muestras->have_posts() ) : $muestras->the_post(); ?>
								<li>
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail( 'slide-peliculas' ); ?>
										<p><?php the_title(); ?></p>
									</a>
								</li>
								<?php
									endwhile;
									endif;
									wp_reset_query();
								?>
					</ul>
				</div>
				<hr>
				<div class="row flexslider slide-peliculas" id="slide-largometrajes">
					<header>
						<h3>Competencia Nacional de Largometrajes "Nuevas Tendencias"</h3>
					</header>
					<ul class="slides">
						<?php if ( $largometrajes->have_posts() and is_home() ) : while ( $largometrajes->have_posts() ) : $largometrajes->the_post(); ?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'slide-peliculas' ); ?>
									<p><?php the_title(); ?></p>
								</a>
							</li>
							<?php
								endwhile;
								endif;
								wp_reset_query();
							?>
					</ul>
				</div>
				<hr>
				<div class="row flexslider slide-peliculas" id="slide-cortometrajes">
					<header>
						<h3>Competencia de Cortometrajes "Radiografía Nacional"</h3>
					</header>
					<ul class="slides">
						<?php if ( $cortometrajes->have_posts() and is_home() ) : while ( $cortometrajes->have_posts() ) : $cortometrajes->the_post(); ?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'slide-peliculas' ); ?>
									<p><?php the_title(); ?></p>
								</a>
							</li>
							<?php
								endwhile;
								endif;
								wp_reset_query();
							?>
					</ul>
				</div>
			</div>
		</main>
	</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
