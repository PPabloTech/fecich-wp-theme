<?php get_header(); ?>
<div class="col-lg-9 col-md-9 columna-contenido">
	<main>
		<header>
			<h1><?php the_title(); ?></h1>
			<?php
				//Empieza el loop. Es porque the_content(); no funciona fuera del loop
				while ( have_posts() ) : the_post();
				if ( has_post_thumbnail() ) : the_post_thumbnail( 'cabecera-articulo', array(
						'class' => 'img-responsive img-rounded',
				));
			?>
		</header>
		<hr>
		<?php
			endif;
			the_content();
			endwhile;
			wp_reset_query();
		?>
	</main>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
