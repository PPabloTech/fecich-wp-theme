<?php
/*
Template name: Peliculas
*/
?>
<?php get_header(); ?>
<div class="col-lg-9 col-md-9 columna-contenido">
	<main>
    <header>
		    <h1><?php the_title(); ?></h1>
    </header>
    <div class="col-lg-4">
       <?php the_post_thumbnail( 'afiche-pelicula', array(
         	'class' => 'img-responsive',
       )); ?>
       <div class="panel panel-default ficha-entradas">
     		<div class="panel-body">
						<h3 class="text-center">Entrada liberada</h3>
						<hr>
						<h4 class="text-center"><strong>Entrada liberada</strong></h4>
						<p class="text-justify">
							Para asistir a las funciones de Quilpué, se deben retirar las entradas en el Domo FECICH (Plaza Eugenio Rengifo).
								<br>
							Para Villa Alemana, se pueden retirar en el Centro Cultural Gabriela Mistral (Santiago #674).
						</p>
        </div>
      </div>
    </div>
    <div class="col-lg-8 ficha-pelicula">
		<?php
		//Empieza el loop. Es porque the_content(); no funciona fuera del loop
			while ( have_posts() ) : the_post();
				//Muestra el contenido
				the_content();
			endwhile;
			wp_reset_query();
		?>
  </div>
	</main>
</div>
<?php get_sidebar('peliculas') ?>
<?php get_footer(); ?>
