# LEEME #

Repositorio de diseño web de FECICH 2015.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Lo que falta ###

* Limite de artículos en categorías (fijar limite de artículos 'por pagina').
* Estilo de pagina para películas (falta añadir cuadro texto para horarios y fecha de exhibición).
* Acordeón para festival.

### Ya listo ###

* Estructura básica.
* Menú responsive.
* Header por default, y con posibilidad de cambio en el panel de WP.
* Header fijo para cuando el diseño se vea en Smartphones.
* Slideshow con artículos en el index (máximo 5 artículos).
* Pie de pagina (imagen estática).
* Estilo articulo (Titulo, imagen destacada).
* Categorías en el index (noticias, largometrajes).
* Sección noticias en index (muestra artículos de dicha categoría).
* Columna con redes sociales (facebook y twitter, falta video de youtube).
* Limite artículos de noticias en index (limite fijo de 3 artículos).
* Slideshow tipo carousel para largometrajes y cortometrajes (funciona como tal, pero falta el contenido).
* Widget en paginas de películas que muestra distintas películas en competencia.

### Lo que incluye este Theme ###

* HTML 5
* CSS 3
* Twitter Bootstrap v3.3.5 instalado en el Theme
* jQuery v1.11.3 del servidor de jQuery
* FlexSlider v2.5.0 instalado en el Theme