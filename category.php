<?php get_header(); ?>
<div class="col-lg-9 col-md-9 columna-contenido">
	<main>
		<h2><?php single_cat_title(); ?></h2>
		<?php
			if ( function_exists('wp_bootstrap_pagination') )
    	wp_bootstrap_pagination();
		?>
		<?php
			if ( have_posts() ) :
		?>
			<div class="row articulos-categoria">
				<?php
					while ( have_posts() ) : the_post();
				?>
				<div class="col-lg-6">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'imagen-categoria' ); ?>
					</a>
					<header>
						<h3><?php the_title(); ?></h3>
					</header>
					<?php the_excerpt(); ?>
				</div>
		<?php
			endwhile;
		?>
		</div>
		<?php
			else: echo "<p>Lo sentimos, no hay articulos que mostrar de dicha categoria.</p>";
			endif;
			wp_reset_query();
		?>
	</main>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
