<?php
/*
Template name: Programación
*/
?>
<?php get_header(); ?>
<style>
	.demo table
	{
		font-family: Arial !important;
		font-size: 10px;
	}
	.demo {
		width:100%;
		border:1px solid #C0C0C0;
		border-collapse:collapse;
		padding:5px;
	}
	.demo th {
		border:1px solid #C0C0C0;
		padding:5px;
		background:#8C8C8C;
		color: #F4F4F4 !important;
		text-align: center;
	}
	.demo td {
		border:1px solid #C0C0C0;
		text-align:center !important;
		padding:5px;
		background:#FFFFFF;
	}
</style>
<div class="col-lg-12 col-md-12 columna-contenido">
	<main>
		<section id="titulo">
		  <h1><?php the_title(); ?></h1>
		</section>
		<div class="datagrid">
			<?php while ( have_posts() ) : the_post();
			the_content();
			endwhile;
			wp_reset_query();
			?>
		</div>
	</main>
</div>
<?php get_footer(); ?>
