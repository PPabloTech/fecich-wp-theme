<?php
$peliculas = new WP_Query(array(
	'post_type'      => 'page',
	'category_name'  => 'largometrajes, cortometrajes, muestras',
	'orderby' => 'rand',
	'showposts' => 5,
));
?>
<aside>
	<div class="col-md-3 col-lg-3 columna-peliculas">
		<div class="panel panel-default">
			<div class="panel-body">
					<div class="row slide-peliculas-sidebar">
						<ul>
	          <h4 class="text-center">Peliculas en exhibición</h4>
						<?php if ( $peliculas->have_posts() ) : while ( $peliculas->have_posts() ) : $peliculas->the_post(); ?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('thumbnail'); ?>
									<p><?php the_title(); ?></p>
								</a>
							</li>
							<?php
								endwhile;
							endif;
							wp_reset_query();
							?>
						</ul>
			  	</div>
	  		</div>
	  	</div>
	</div>
</aside>
