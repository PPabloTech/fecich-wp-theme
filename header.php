<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php bloginfo('name') ?></title>
		<link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		<?php wp_head(); ?>
	</head>
	<body>
		<script>
			(function(d, s, id)
			{
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.4";
				fjs.parentNode.insertBefore(js, fjs);
			}
			(document, 'script', 'facebook-jssdk'));
		</script>
		<div class="container">
			<br>
			<header id="cabecera-sitio-web">
				<div id="logo">
					<img src="<?php header_image(); ?>" class="img-responsive hidden-xs" alt="Header FECICH">
					<img src="<?php echo IMAGES ?>/header-small.png" class="img-responsive visible-xs-block" alt="Header FECICH">
				</div>
				<br>
				<nav role="navigation" class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" data-target="#navmenu" data-toggle="collapse" class="navbar-toggle">
							<span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
		      			</button>
					</div>
					<?php wp_nav_menu(array(
						'container'       => 'div',
						'container_id'    => 'navmenu',
						'container_class' => 'navbar-collapse collapse',
						'menu_class'      => 'nav navbar-nav',
						'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
						'walker'          => new wp_bootstrap_navwalker(),
					)); ?>
				</nav>
			</header>
			<div class="row" id="contenido">
